# Description
Sntence embedding using power means concatenated

# Credits
This project uses SentEval and InferSent which is used for comparasion
https://github.com/facebookresearch/SentEval
https://github.com/facebookresearch/InferSent

# Data Source

## InferSent data dowlnoaded using InferSent readme
https://github.com/facebookresearch/InferSent
## SICK dataset
http://alt.qcri.org/semeval2014/task1/data/uploads/
